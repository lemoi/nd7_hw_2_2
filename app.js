const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": true}));

function findUser(users, id) {
	let user = users.find(elem => elem.id == id);
	return user === undefined ? null : user;
}

function getUsers(limit, offset = 1, fields) {
	let content = fs.readFileSync("users.json", "utf8");
	let users = JSON.parse(content);

	users = users.splice(offset - 1 || 0, limit || users.length);

	let arrFields = String(fields).split(",");
	if(fields !== undefined && arrFields.length > 0) {
		users = users.map(elem => {
			let user = {};
  		for(let field of arrFields) {
  			if(field in elem) {
  			  user[field] = elem[field];
  			}
  		}
  		if(Object.keys(user).length == 0) {
  		  user = elem;
  		}
  		return user;
		});
	};

	return users;
};

function createUser(name, score) {
	let data = fs.readFileSync("users.json", "utf8");
	let users = JSON.parse(data);

	let id = users.reduce((acc, elem) => acc > elem.id ? acc : elem.id , 0) + 1;
	let user = {id: id, name: name, score: score};
	users.push(user);

	data = JSON.stringify(users);
	fs.writeFileSync("users.json", data);

	return user;
};

function getUser(id) {
	let content = fs.readFileSync("users.json", "utf8");
	let users = JSON.parse(content);

	let user = findUser(users, id);

	return user;
};

function updateUser(id, name, score) {
	let data = fs.readFileSync("users.json", "utf8");
	let users = JSON.parse(data);

	let user = findUser(users, id);
	if(user) {
		user.name = name;
		user.score = score;

		let data = JSON.stringify(users);
		fs.writeFileSync("users.json", data);
	};

	return user;
}

function deleteUser(id) {
	let content = fs.readFileSync("users.json", "utf8");
	let users = JSON.parse(content);

	let user = null;
	let index = users.findIndex(elem => elem.id == id);
	if(index > -1) {
		user = users.splice(index, 1)[0];

		let data = JSON.stringify(users);
		fs.writeFileSync("users.json", data);
	}

	return user;
};

function deleteUsers() {
	let users = [];
	let data = JSON.stringify(users);
	fs.writeFileSync("users.json", data);

	return users;
};

const rtAPIv1 = express.Router();

rtAPIv1.get("/users/", function(req, res) {
	let users = getUsers(req.query.limit, req.query.offset, req.query.fields);
	res.json(users)
});

rtAPIv1.post("/users/", function(req, res) {
	if(!req.body || !req.body.name) return res.sendStatus(400);
	let users = createUser(req.body.name, req.body.score);
	res.json(users);
});

rtAPIv1.get("/users/:id", function(req, res) {
	let user = getUser(req.params.id);
	if(user) {
		res.json(user);
	} else {
		res.status(404).send();
	}
});

rtAPIv1.put("/users/:id", function(req, res) {
	if(!req.body) return res.sendStatus(400);

	let user = updateUser(req.params.id, req.body.name, req.body.score);
	if(user) {
		res.json(user);
	} else {
		res.status(404).send();
	}
});

rtAPIv1.delete("/users/:id", function(req, res) {
	let user = deleteUser(req.params.id);
	if(user) {
		res.json(user);
	} else {
		res.status(404).send();
	}
});

app.use("/api/v1", rtAPIv1);


let RPC = {
	getUsers: function(params, callback) {
		let users = getUsers(params.limit, params.offset, params.fields);
		callback(null, users);
	},

	createUser: function(params, callback) {
		let users = createUser(params.name, params.score);
		callback(null, users);
	},

	getUser: function(params, callback) {
		let user = getUser(params.id);
		callback(null, user);
	},

	updateUser: function(params, callback) {
		let user = updateUser(params.id, params.name, params.score);
		if(user) {
			callback(null, user);
		} else {
			callback(new Error("User not found"))
		}
	},

	deleteUser: function(params, callback) {
		let user = deleteUser(params.id);
		if(user) {
			callback(null, user);
		} else {
			callback(new Error("User not found"))
		}
	},

	deleteUsers: function(params, callback) {
		let users = deleteUsers();
		callback(null, users);
	}
}

const rpcAPIv2 = express.Router();

rpcAPIv2.post("/rpc", function(req, res) {
	const method = RPC[req.body.method];
	if(typeof method != "function") {
		res.status(400).send();
	};
	try {
		method(req.body.params, function(error, result) {
			if(error) {
				console.error(error);
				res.status(500).send();
			}
			res.json(result);
		});
	} catch(error) {
		console.error(error);
		res.status(500).send();
	}
});

app.use("/api/v2", rpcAPIv2);

app.listen(3000, () => { console.log("Ожидаем подключения")});

module.exports = app;