const request = require('supertest');

describe('Интеграционные тесты для задания 2.2 «Создаем простое API, используя express».', () => {

	let app = require('../app.js');

    let lastId;

    before(done => {
		request(app)
			.get('/api/v1/users')
			.expect(200)
			.end(function(err, res) {
				if (err) throw err;
				lastId = res.body[res.body.length - 1].id;
				done();
			});
    });

	it('Получения списка пользователей', (done) => {
		request(app)
			.get('/api/v1/users')
			.expect(200)
			.end((err, res) => {
				if (err) throw err;
	            console.log('Пользователей в списке: ', res.body.length);
				done();
			});
	});

    describe("Проверяем API создания пользователя", () => {

		it('Создание пользователя с корректными свойствами', (done) => {
			request(app)
				.post('/api/v1/users')
				.send({name: 'Пикачу', score: 5})
				.expect(200)
				.end((err, res) => {
					if (err) throw err;
					console.log(res.body);
					lastId++;
					done();
				});
		});

		it('Создание пользователя без тела запроса приводит к ошибке 400', (done) => {
			request(app)
				.post('/api/v1/users')
				.expect(400, done);
		});

		it('Создание пользователя с пустым именем недопустимо (ошибка 400)', (done) => {
			request(app)
				.post('/api/v1/users')
				.expect(400, done);
		});

	});

    describe("Проверяем API удаление пользователя", () => {

		it('Удаление пользователя с существующим ID', (done) => {

			console.log('Удаление пользователя с ID', lastId);

			request(app)
				.delete('/api/v1/users/' + lastId)
				.expect(200)
				.end((err, res) => {
					if (err) throw err;
					console.log(res.body);
					lastId--;
					done();
				});

		});

		it('Удаление несуществующего пользователя приводит к ошибке 404', (done) => {

			userId = lastId + 10;
			console.log('Удаление пользователя с ID', userId);

			request(app)
				.delete('/api/v1/users/' + userId)
				.expect(404, done);

		});

	});

	/*
	const app = require('../app.js');
	const http = require('http');
	const assert = require('assert');
    const querystring = require('querystring');
	let options;
    let lastId;
    const PORT = 3000;

	before(() => {
		http.get(`http://localhost:${PORT}/api/v1/users`, response => {
            response.setEncoding('utf8');
            let answer = '';
            response.on('data', (chunk) => answer += chunk);
            response.on('end', () => {
	            let parsedData = JSON.parse(answer);
	            lastId = parsedData[parsedData.length - 1].id;
            });
		});
	});

	beforeEach(() => {
		options = {
			method: 'GET',
			host: 'localhost',
			port: PORT,
			path: '/api/v1/users'
		};
	});

	it('Получения списка пользователей', (done) => {

		http.request(options, response => {
			assert.equal(response.statusCode, 200);

            response.setEncoding('utf8');
            let answer = '';
            response.on('data', (chunk) => answer += chunk);
            response.on('end', () => {
	            let parsedData = JSON.parse(answer);
	            console.log('Пользователей в списке: ', parsedData.length);
	            done();
            });
		}).end();

	});

	it('Создание пользователя', (done) => {

        let data = querystring.stringify({
            name: 'Пикачу',
            score: 5
        });

		options.method = 'POST';
		options.headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(data)
        };

		let req = http.request(options, response => {
			assert.equal(response.statusCode, 200);

            response.setEncoding('utf8');
            let answer = '';
            response.on('data', (chunk) => answer += chunk);
            response.on('end', () => {
	            let parsedData = JSON.parse(answer);
	            console.log(parsedData);
	            lastId++;
	            done();
            });
		});
		req.write(data);
		req.end();

	});

	it('Удаление пользователя', (done) => {

		console.log('Удаление пользователя с ID', lastId);

		options.method = 'DELETE';
		options.path = '/api/v1/users/' + lastId;

		let req = http.request(options, response => {
			assert.equal(response.statusCode, 200);

            response.setEncoding('utf8');
            let answer = '';
            response.on('data', (chunk) => answer += chunk);
            response.on('end', () => {
	            let parsedData = JSON.parse(answer);
	            console.log(parsedData);
	            done();
            });
		}).end();

	});
	*/

});